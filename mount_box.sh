#!/bin/sh

# Install devfs. (initial only)
# sudo apt-get install davfs2

# mkdir ~/Box

sudo mount -t davfs https://dav.box.com/dav /home/`whoami`/Box/


# If you want to mount without root permission...

# add user to davfs2 group
# sudo usermod -aG davfs2 kiku

# add following to /etc/fstab
# https://dav.box.com/dav /home/kiku/Box/ davfs user,noauto 0 0

# and issue this command to mount
# mount /home/kiku/Box

